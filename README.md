# Goodboy Backend Dev Test

REST API project for Goodboy backend coding test.

You will be required to implement a basic API for a multiplayer online game that provides user authentication and allows users to perform certain operations.

We have provided a [JSON manifest with a list of items within the game](./json/items.json), including masks (heads) and robes (bodies). This list of items should be used by the API and could be stored in a database or kept in a JSON file. 

Registered users should get 200 coins and no owned items by default (empty inventory), and then should be able to purchase these items using the "Buy an Item" endpoint. 

Every time a user makes a purchase, the cost of the purchased item(s) should be deducted from the user coins balance, ie: A user with 200 coins should not be able to buy an item that costs 300 coins.

## API Requirements

The API will need a database to store it's data. You can use any database management system (either relational or NoSQL) of your choice (eg: MySQL, PostgreSQL, MongoDB or DynamoDB would all be valid choices).

We require the API to be built with NodeJS (we recommend using Express.JS framework), and you can use as many NPM libraries as you want.

You are expected to implement the endpoints defined in the 'API Reference' section. For users authentication (`refreshToken` and `sessionToken`), we recommend using JWT tokens (https://jwt.io).

## API Reference

### Register a User

```shell  
curl http://{apiUrl}/api/users/register 
  -H "Content-Type: application/json" 
  -X POST 
```

When a new user access the game, the game client will use this endpoint to create a new user. The API will need to create a user record and assign a random display name to that user. The API will then return a `refreshToken` that can be used to identify this user.

The `refreshToken` is required to start a new session, or request a `sessionToken` (needed for most interactions with the API), and can be used in the `Start User Session` endpoint request to start a user session.

> The above command returns JSON structured like this:

```json
{
  "displayName": "RandomDisplayName",
  "refreshToken": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImY1ZTZjNzgxLTU2MzAtNGFlMy1hYWFiLTBkOWE5MzY1MzAyMiIsImV4cGlyZSI6NjMwNzIwMDAsImlhdCI6MTU0ODg5Mjc2NiwiZXhwIjoxNjExOTY0NzY2LCJhdWQiOiJodHRwOi8vbG9jYWxob3N0IiwiaXNzIjoiR29vZGJveSBEaWdpdGFsIiwic3ViIjoic29tZXVzZXIifQ.L-O0ToWAjpUHv8SUjNK2A8x1jvT3Z0m6_W4lUA_tFTeMS43aLqYIWyb7vL9EhRu0kGcR9TwteTSjo8A45TeasA"
}
```

#### HTTP Request

`POST https://{apiUrl}/api/users/register`

#### Response

Key | Description
--------- | -----------
displayName | Display name for newly created user.
refreshToken | JWT token required for the 'Start User Session' endpoint request

### Start User Session

```shell
curl https://{apiUrl}/api/users/login
  -H "Content-Type: application/json" 
  -X POST 
  -d '{"refreshToken":"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImY1ZTZjNzgxLTU2MzAtNGFlMy1hYWFiLTBkOWE5MzY1MzAyMiIsImV4cGlyZSI6NjMwNzIwMDAsImlhdCI6MTU0ODg5Mjc2NiwiZXhwIjoxNjExOTY0NzY2LCJhdWQiOiJodHRwOi8vbG9jYWxob3N0IiwiaXNzIjoiR29vZGJveSBEaWdpdGFsIiwic3ViIjoic29tZXVzZXIifQ.L-O0ToWAjpUHv8SUjNK2A8x1jvT3Z0m6_W4lUA_tFTeMS43aLqYIWyb7vL9EhRu0kGcR9TwteTSjo8A45TeasA"}'
```

Currently, users can only access the application using a valid `refreshToken`. A valid `refreshToken` is required to identify the user and start a session.

After a user is successfully logged in, a (JWT) `sessionAccessToken` will be provided.

> The above command returns JSON structured like this:

```json
{
  "error": false,
  "sessionAccessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
  "expiresAt": "1516239022",
  "issuedAt": "1516239022",
  "displayName": "RandomDisplayName"
}
```

> If the user tries to start a session with an invalid refreshToken, the response will be similar to this:

```json
{
  "error": true,
  "errorMessage": "Invalid token",
  "sessionAccessToken": "",
  "expiresAt": "",
  "issuedAt": "",
  "displayName": ""
}
```

#### HTTP Request

`POST https://{apiUrl}/api/users/login`

#### JSON Body Parameters

Parameter | Description
--------- | -----------
refreshToken | JWT token used to authenticate guest users.

### Response

Key | Description
--------- | -----------
sessionAccessToken | JWT access token that will identify the current user and will be required for certain API requests restricted to logged in users. 
expiresAt | Date when access token will expire and user will need to request a new one.
issuedAt | Date when access token was issued.
displayName | Display name for logged in user.

### Get User Data

```shell
curl https://{apiUrl}/api/users/user
  -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
```

Returns data for the current user.

This endpoint expects for a valid user sessionAccessToken to be included in a header that looks like the following:

`Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c`.

If an invalid user token is provided in the Authorization header, the server will respond a `401` HTTP error code.

```json
{
  "displayName": "RandomDisplayName",
  "userExp": "100",
  "coins": "200",
  "inventory": [
    "abddae38-4980-44da-a05c-bcd203fc96f8",
    "om87431d-0e65-8e11-4615-875412dffd38",
    "99f4e6bc-e4dc-4421-b52f-1039101654b7"
  ]
}
```

#### HTTP Request

`GET https://{apiUrl}/api/users/user`

#### Response

Key | Description
--------- | -----------
displayName | User display name.
userExp | Points of experience acquired by the user.
coins | Coins possessed by the user.
inventory | Array of inventory item IDs that the user owns.

### Buy an Item

```shell
curl https://{apiUrl}/api/inventory/buy
  -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
  -X POST
  -d '{"itemId":"abddae38-4980-44da-a05c-bcd203fc96f8"}'
```

Buy an item from the shop. Available items are defined in the [JSON items manifest provided in this repository](./json/items.json).

Every time a user makes a purchase, the cost of the purchased item(s) should be deducted from the user coins balance, ie: A user with 200 coins should not be able to buy an item that costs 300 coins.

This endpoint expects for a valid user token to be included in a header that looks like the following:

`Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c`

If an invalid user token is provided in the Authorization header, the server will respond a `401` HTTP error code.

> The above command returns JSON structured like this:

```json
{
  "error": false,
  "errorMessage": "",
  "coinBalance": "50"
}
```

> If the user coin balance is not enough to buy an item, the response will be similar to this:

```json
{
  "error": true,
  "errorMessage": "You don't have enough coins for this upgrade",
  "coinBalance": "50"
}
```

#### JSON Body Parameters

Parameter | Description
--------- | -----------
itemId | ID of the selected item or upgrade (eg: `abddae38-4980-44da-a05c-bcd203fc96f8`).

#### Response

Key | Description
--------- | -----------
success | Boolean returning whether the item was actually bought or not.
message | Message to display to the user. 
coinBalance | Balance of coins of the active user after the last purchase.
